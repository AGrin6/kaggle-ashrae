import sys
import copy
import time
import random
import math
import numpy
from sklearn.ensemble import GradientBoostingRegressor

# TODO feature precomputation to get mid year or list of possible values etc
# mean/median etc

# Add day of the week etc

###############################################################################
# Add building age taking into account current date
# Add cyclic_encoder
# (thanks, https://www.kaggle.com/srbestha/ashare-light-gbm)
# Add separate models for different sites
###############################################################################

TRAIN_SAMPLES_NUMBER =  50000
VALID_SAMPLES_NUMBER = 200000
DATETIME_SPLIT_THRESHOLD = '2016-04-01 00:00:00'

ESTIMATORS_NUMBER =  200

SITE_0_CORRECTION = 0.2931

PRIMARY_USE_LIST = ['Manufacturing/industrial', 'Retail', 'Education', 'Healthcare',
                    'Lodging/residential', 'Public services', 'Entertainment/public assembly',
                    'Food sales and service', 'Other', 'Religious worship', 'Utility',
                    'Technology/science', 'Services', 'Office', 'Warehouse/storage', 'Parking']

# def load_data(train_number):
#   list_of_instances = []
#   list_of_labels = []
# 
#   with open('./data/train.csv') as input_stream:
#     header_line = input_stream.readline()
#     columns = header_line.strip().split(',')
#     for line in input_stream:
#       new_instance = dict(zip(columns[:-1], line.split(',')[:-1]))
#       new_label = float(line.split(',')[-1])
#       list_of_instances.append(new_instance)
#       list_of_labels.append(new_label)
#       if len(list_of_instances) == instances_number:
#         break
#   return list_of_instances, list_of_labels


def load_data(train_number, valid_number, datetime_threshold):
  with open('./data/train.csv') as input_stream:
    lines_counter = 0
    for line in input_stream:
      if line.split(',')[2] == datetime_threshold:
        break
      lines_counter += 1
    train_lines = lines_counter
    for line in input_stream:
      lines_counter += 1

  train_instances = []
  train_labels = []
  train_sampling = set(random.sample(range(train_lines), train_number))
  # print(train_lines, lines_counter)
  valid_sampling = set(random.sample(range(train_lines, lines_counter), valid_number))
  with open('./data/train.csv') as input_stream:
    header_line = input_stream.readline()
    columns = header_line.strip().split(',')
    lines_counter = 0
    for line in input_stream:
      if lines_counter in train_sampling:
        new_instance = dict(zip(columns[:-1], line.split(',')[:-1]))
        new_label = float(line.split(',')[-1])
        train_instances.append(new_instance)
        train_labels.append(new_label)
      if line.split(',')[2] == datetime_threshold:
        break
      lines_counter += 1

    valid_instances = []
    valid_labels = []
    for line in input_stream:
      if lines_counter in valid_sampling:
        new_instance = dict(zip(columns[:-1], line.split(',')[:-1]))
        new_label = float(line.split(',')[-1])
        valid_instances.append(new_instance)
        valid_labels.append(new_label)
      lines_counter += 1
  return train_instances, train_labels, valid_instances, valid_labels

# a, b, c, d = load_data_new(10, 15, '2016-01-15 00:00:00')
# print(a)
# print(b)
# print(c)
# print(d)
# sys.exit()

def load_building_data():
  result = dict()

  building_to_site = dict()
  building_to_use = dict()
  building_to_space = dict()
  building_to_year = dict()
  building_to_floor = dict()

  with open('./data/building_metadata.csv') as input_stream:
    header_line = input_stream.readline()
    for line in input_stream:
      site_id = line.split(',')[0]
      building_id = line.split(',')[1]
      primary_use = line.split(',')[2]
      square_feet = line.split(',')[3]
      year_built = line.split(',')[4]
      floor_count = line[:-1].split(',')[5]
      building_to_site[building_id] = site_id
      building_to_use[building_id] = primary_use
      building_to_space[building_id] = square_feet
      building_to_year[building_id] = year_built
      building_to_floor[building_id] = floor_count
      # print(site_id, building_id, primary_use, square_feet, year_built, floor_count)
      # input()

  result['building_to_site'] = building_to_site
  result['building_to_use'] = building_to_use
  result['building_to_space'] = building_to_space
  result['building_to_year'] = building_to_year
  result['building_to_floor'] = building_to_floor
  return result


def load_weather_data(dataset_type):
  result = dict()

  to_air_temperature = dict()
  to_cloud_coverage = dict()
  to_dew_temperature = dict()
  to_precip_depth = dict()
  to_sea_pressure = dict()
  to_wind_direction = dict()
  to_wind_speed = dict()

  if dataset_type == 'train':
    file_name = './data/weather_train.csv'
  elif dataset_type == 'test':
    file_name = './data/weather_test.csv'
  else:
    raise ValueError

  with open(file_name) as input_stream:
    header_line = input_stream.readline()
    for line in input_stream:
      site_id = line.split(',')[0]
      timestamp = line.split(',')[1]
      air_temperature = line.split(',')[2]
      cloud_coverage = line.split(',')[3]
      dew_temperature = line.split(',')[4]
      precip_depth = line.split(',')[5]
      sea_pressure = line.split(',')[6]
      wind_direction = line.split(',')[7]
      wind_speed = line.split(',')[8]

      if not site_id in to_air_temperature:
        to_air_temperature[site_id] = dict()
      to_air_temperature[site_id][timestamp] = air_temperature

      if not site_id in to_cloud_coverage:
        to_cloud_coverage[site_id] = dict()
      to_cloud_coverage[site_id][timestamp] = cloud_coverage

      if not site_id in to_dew_temperature:
        to_dew_temperature[site_id] = dict()
      to_dew_temperature[site_id][timestamp] = dew_temperature

      if not site_id in to_precip_depth:
        to_precip_depth[site_id] = dict()
      to_precip_depth[site_id][timestamp] = precip_depth

      if not site_id in to_sea_pressure:
        to_sea_pressure[site_id] = dict()
      to_sea_pressure[site_id][timestamp] = sea_pressure

      if not site_id in to_wind_direction:
        to_wind_direction[site_id] = dict()
      to_wind_direction[site_id][timestamp] = wind_direction

      if not site_id in to_wind_speed:
        to_wind_speed[site_id] = dict()
      to_wind_speed[site_id][timestamp] = wind_speed

  result['to_air_temperature'] = to_air_temperature
  result['to_cloud_coverage'] = to_cloud_coverage
  result['to_dew_temperature'] = to_dew_temperature
  result['to_precip_depth'] = to_precip_depth
  result['to_sea_pressure'] = to_sea_pressure
  result['to_wind_direction'] = to_wind_direction
  result['to_wind_speed'] = to_wind_speed

  return result


# Correct labels to fix organizers' mistake. Detailed discussion here:
# https://www.kaggle.com/c/ashrae-energy-prediction/discussion/119261
def correct_labels(list_of_instances, list_of_labels, building_data):
  for i in range(len(list_of_instances)):
    if building_data['building_to_site'][list_of_instances[i]['building_id']] == '0':
      list_of_labels[i] *= SITE_0_CORRECTION
  return list_of_instances, list_of_labels


def get_meter(instance):
  return [0] * int(instance['meter']) + [1] + [0] * (4 - int(instance['meter']) - 1)


def get_site(instance, building_to_site):
  site_id = int(building_to_site[instance['building_id']])
  result = [0] * site_id + [1] + [0] * (16 - site_id - 1)
  return result


def get_use(instance, building_to_use):
  primary_use = building_to_use[instance['building_id']]
  if primary_use in PRIMARY_USE_LIST:
    use_index = PRIMARY_USE_LIST.index(primary_use)
    result = [0] * use_index + [1] + [0] * (len(PRIMARY_USE_LIST) - use_index - 1)
  else:
    result = [0] * len(PRIMARY_USE_LIST)
  return result


def get_space(instance, building_to_space):
  space = int(building_to_space[instance['building_id']])
  return [space]


def get_year(instance, building_to_year):
  if building_to_year[instance['building_id']] == '':
    year_built = 1990
  else:
    year_built = int(building_to_year[instance['building_id']])
  return [year_built]


def get_floor(instance, building_to_floor):
  if building_to_floor[instance['building_id']] == '':
    floor_count = 5
  else:
    floor_count = int(building_to_floor[instance['building_id']])
  return [floor_count]


def get_air_temperature(instance, additional_data):
  site_id = additional_data['building_data']['building_to_site'][instance['building_id']]
  to_air_temperature = additional_data['train_weather_data']['to_air_temperature']
  # print(to_air_temperature[site_id])
  if (instance['timestamp'] in to_air_temperature[site_id]
      and to_air_temperature[site_id][instance['timestamp']] != ''):
    result = [float(to_air_temperature[site_id][instance['timestamp']])]
  else:
    result = [25.0]
  return result


def get_cloud_coverage(instance, additional_data):
  site_id = additional_data['building_data']['building_to_site'][instance['building_id']]
  to_cloud_coverage = additional_data['train_weather_data']['to_cloud_coverage']
  # forgiveness and permission... hmm...
  if (instance['timestamp'] in to_cloud_coverage[site_id]
      and to_cloud_coverage[site_id][instance['timestamp']] != ''):
    result = [float(to_cloud_coverage[site_id][instance['timestamp']])]
  else:
    result = [6.0]
  return result


def get_dew_temperature(instance, additional_data):
  site_id = additional_data['building_data']['building_to_site'][instance['building_id']]
  to_dew_temperature = additional_data['train_weather_data']['to_dew_temperature']
  # forgiveness and permission... hmm...
  if (instance['timestamp'] in to_dew_temperature[site_id]
      and to_dew_temperature[site_id][instance['timestamp']] != ''):
    result = [float(to_dew_temperature[site_id][instance['timestamp']])]
  else:
    result = [25]
  return result


def get_precip_depth(instance, additional_data):
  site_id = additional_data['building_data']['building_to_site'][instance['building_id']]
  to_precip_depth = additional_data['train_weather_data']['to_precip_depth']
  # forgiveness and permission... hmm...
  if (instance['timestamp'] in to_precip_depth[site_id]
      and to_precip_depth[site_id][instance['timestamp']] != ''):
    result = [float(to_precip_depth[site_id][instance['timestamp']])]
  else:
    result = [0.0]
  return result


def get_sea_pressure(instance, additional_data):
  site_id = additional_data['building_data']['building_to_site'][instance['building_id']]
  to_sea_pressure = additional_data['train_weather_data']['to_sea_pressure']
  # forgiveness and permission... hmm...
  if (instance['timestamp'] in to_sea_pressure[site_id]
      and to_sea_pressure[site_id][instance['timestamp']] != ''):
    result = [float(to_sea_pressure[site_id][instance['timestamp']])]
  else:
    result = [0.0]
  return result


def get_wind_direction(instance, additional_data):
  site_id = additional_data['building_data']['building_to_site'][instance['building_id']]
  to_wind_direction = additional_data['train_weather_data']['to_wind_direction']
  # forgiveness and permission... hmm...
  if (instance['timestamp'] in to_wind_direction[site_id]
      and to_wind_direction[site_id][instance['timestamp']] != ''):
    result = [float(to_wind_direction[site_id][instance['timestamp']])]
  else:
    result = [0.0]
  return result


def get_wind_speed(instance, additional_data):
  site_id = additional_data['building_data']['building_to_site'][instance['building_id']]
  to_wind_speed = additional_data['train_weather_data']['to_wind_speed']
  # forgiveness and permission... hmm...
  if (instance['timestamp'] in to_wind_speed[site_id]
      and to_wind_speed[site_id][instance['timestamp']] != ''):
    result = [float(to_wind_speed[site_id][instance['timestamp']])]
  else:
    result = [1.5]
  return result


def to_sample(instance, additional_data):
  return (get_meter(instance)
			    + get_site(instance, additional_data['building_data']['building_to_site'])
          + get_use(instance, additional_data['building_data']['building_to_use'])
          + get_space(instance, additional_data['building_data']['building_to_space'])
          + get_year(instance, additional_data['building_data']['building_to_year'])
          + get_floor(instance, additional_data['building_data']['building_to_floor'])
          + get_air_temperature(instance, additional_data)
          # + get_cloud_coverage(instance, additional_data)
          + get_dew_temperature(instance, additional_data)
          # + get_precip_depth(instance, additional_data)
          # + get_sea_pressure(instance, additional_data)
          )


def to_interim_label(label):
  return math.log(label + 1)


def to_final_label(interim_label):
  return math.exp(interim_label) - 1


if __name__ == '__main__':
  time_start = time.time()
  print('Train samples number: {0}'.format(TRAIN_SAMPLES_NUMBER))
  print('Valid samples number: {0}'.format(VALID_SAMPLES_NUMBER))
  print('Datetime split threshold: {0}'.format(DATETIME_SPLIT_THRESHOLD))
  print('Estimators number: {0}'.format(ESTIMATORS_NUMBER))
  building_data = load_building_data()
  train_weather_data = load_weather_data('train')
  additional_data = {'building_data':building_data, 'train_weather_data':train_weather_data}
  # list_of_instances, list_of_labels = load_data(instances_number=200000)
  # list_of_instances, list_of_labels = correct_labels(list_of_instances, list_of_labels,
  #                                                    building_data)
  train_instances, train_labels, valid_instances, valid_labels = load_data(
                              TRAIN_SAMPLES_NUMBER, VALID_SAMPLES_NUMBER, DATETIME_SPLIT_THRESHOLD)
  print('Loading data completed {0:.2f}'.format(time.time() - time_start))
  # list_of_samples = list(map(lambda x:to_sample(x, additional_data), list_of_instances))
  train_samples = list(map(lambda x:to_sample(x, additional_data), train_instances))
  # train_samples = list_of_samples[:TRAIN_SAMPLES_NUMBER]
  train_labels = list(map(to_interim_label, train_labels))

  model = GradientBoostingRegressor(learning_rate=1.0, n_estimators=ESTIMATORS_NUMBER,
                                    random_state=0)
  model.fit(numpy.array(train_samples), numpy.array(train_labels))
  print('Training completed {0:.2f}'.format(time.time() - time_start))

  valid_samples = list(map(lambda x:to_sample(x, additional_data), valid_instances))
  # validation_samples = list_of_samples[TRAIN_SAMPLES_NUMBER:]
  valid_labels = list(map(to_interim_label, valid_labels))

  squared_errors = []
  for sample, label in zip(valid_samples, valid_labels):
    prediction = model.predict(numpy.array(sample).reshape(1, -1))[0]
    squared_errors.append((prediction - label) ** 2)
  mean_squared_error = math.sqrt(sum(squared_errors) / len(squared_errors))
  print('Validation completed {0:.2f}'.format(time.time() - time_start))
  print('Mean Squared Error: {0:.8f}'.format(mean_squared_error))


